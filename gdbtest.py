import gdb
import pdb
import re

def eventprinter(event):
    with open("testlog.txt", "a") as handle:
        print(event.__dict__)
        #for bp in event.breakpoints:
            #print(bp.location)
        current_frame = gdb.selected_frame()
        print(current_frame.name())
        current_block = current_frame.block()
        handle.write("{}\n".format(current_block.function))
        for symbol in current_block:
            if symbol.is_argument:
                handle.write("{}: {}\n".format(symbol.name, symbol.value(current_frame)))
        handle.write("global block\n")
        for symbol in current_block.superblock:
            break
            #pdb.set_trace()
            if symbol.is_variable:
                handle.write("Variable {}: {}\n".format(symbol.name, symbol.value(current_frame)))
            elif symbol.is_function:
                handle.write("Function {}: {}\n".format(symbol.name, symbol.value(current_frame)))
            elif  symbol.is_constant:
                handle.write("Constant {}: {}\n".format(symbol.name, symbol.value(current_frame)))
                
        handle.write("\n")
    #gdb.flush()
    gdb.execute("c")


def setup_breakpoints():
    newbp = gdb.Breakpoint("eventLoop")
    
def print_objfiles():
    for objfile in gdb.objfiles():
        print(objfile.filename)
        ps = objfile.progspace

def print_progspaces():
    for ps in gdb.progspaces():
        print(ps.filename)
        print("printing objfiles")
        for objfile in ps.objfiles():
            print(objfile.lookup_static_symbol("*"))
        ps = objfile.progspace

def print_funcs():
    funcs = gdb.execute("info functions", to_string=True)
    track = False
    funcs_to_break = []
    filename = ""
    with open("funcs.txt", "w") as handle:
        for fun in funcs.split("\n"):
            #pdb.set_trace()
            if fun.strip():
                if "File" in fun:
                    if "/home/aerrae/eventHandlerDemo/src/eventmanager.cpp" in fun:
                        filename = fun.split("/")[-1][:-1]
                        handle.write("{}\n".format(fun))
                        track = True
                    else:
                        track = False
                elif track:
                    handle.write("{}\n".format(fun))
                    #print(fun)
                    full_funcname = fun.split()[2]
                    funcname = re.split(r"[\[\(]",full_funcname)[0]
                    #print(funcname)
                    funcs_to_break.append("{}:{}".format(filename, funcname))
                    #break
    #for func in funcs_to_break:
        #print("adding breakpoint to {}".format(func))
        #gdb.Breakpoint(func)
        #gdb.flush()
    gdb.execute("rbreak pwlogic.cpp:.*")
def print_symtab():
    symtab = gdb.Symtab_and_line.symtab
    objfile = symtab.objfile
    for symbol in objfile:
        print("{}: {}\n".format(symbol.name, symbol.value(current_frame)))

gdb.execute("set python print-stack full")
gdb.execute("set pagination off")
with open("testlog.txt", "w") as handle:
    handle.write("LogStart\n")
#setup_breakpoints()
gdb.events.stop.connect(eventprinter)
print_progspaces()
print_funcs()
gdb.execute("run")